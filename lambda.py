import urllib
from typing import Any

from logger import log


def handle_single_upload(record: Any) -> None:
    s3_key = urllib.parse.unquote(record['s3']['object']['key'])
    log.info(s3_key)


def lambda_handler(event: Any, context: Any) -> None:
    for record in event['Records']:
        handle_single_upload(record)
